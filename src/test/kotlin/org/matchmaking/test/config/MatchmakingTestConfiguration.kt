package org.matchmaking.test.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan

@TestConfiguration
@ComponentScan(basePackages = ["org.matchmaking"])
@EnableAutoConfiguration
open class MatchmakingTestConfiguration