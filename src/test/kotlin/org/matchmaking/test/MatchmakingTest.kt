package org.matchmaking.test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.matchmaking.config.MatchmakingDbConfig
import org.matchmaking.db.dao.UserRepository
import org.matchmaking.service.GroupMakingService
import org.matchmaking.service.UserService
import org.matchmaking.test.config.MatchmakingTestConfiguration
import org.matchmaking.util.Logged
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = [
    MatchmakingDbConfig::class,
    MatchmakingTestConfiguration::class
])
@TestMethodOrder( value = MethodOrderer.OrderAnnotation::class)
open class MatchmakingTest(
    @Autowired var userRepo  : UserRepository,
    @Autowired val userService: UserService,
    @Autowired val makeService : GroupMakingService
) : Logged {

    private val iterations : Int = 2

    @Test
    @Order(1)
    @Transactional
    open fun testInsert() {
        log().debug("=====TEST [ 1 ] =====")
        assertEquals( userService.addUser("test", 50.0, 50.0),
            "UserAdded")
        assertNotNull( userRepo.findByName("test"))
    }

    @Test
    @Order(2)
    @Transactional
    open fun testRepeatedInsert() {
        log().debug("=====TEST [ 2 ] =====")
        userService.addUser("test", 50.0, 50.0)
        assertEquals( userService.addUser("test", 50.0, 50.0),
            "UserAlreadyExists")
    }

    @Test
    @Order(3)
    @Transactional
    open fun testGroupCreating() {
        log().debug("=====TEST [ 3 ] =====")
        userService.addUser("test", 49.0, 49.0 )
        makeService.formGroups()
        val user = userRepo.findByName("test")

        assertNotNull(user)
        assertNull(user!!.groupRef)

        userService.addUser("test2", 50.0, 50.0 )
        makeService.formGroups()
        val otherUser = userRepo.findByName("test2")

        assertNotNull(otherUser)
        assertNull(otherUser!!.groupRef)

        userService.addUser("test3", 51.0, 51.0 )
        makeService.formGroups()
        val anotherUser = userRepo.findByName("test3")

        assertNotNull(anotherUser)
        assertNotNull(anotherUser!!.groupRef)

        assertEquals(user.groupRef, otherUser.groupRef)
        assertEquals(anotherUser.groupRef, otherUser.groupRef)
    }

    @Test
    @Order(4)
    @Transactional
    open fun testGroupForming() {
        log().debug("=====TEST [ 4 ] =====")
        userService.addUser("test1", 30.0, 30.0 )
        userService.addUser("test2", 33.0, 33.0 )
        userService.addUser("test3", 43.0, 43.0 )

        val lowSkillUser = userRepo.findByName("test1")
        val midSkillUser = userRepo.findByName("test2")
        val highSkillUser = userRepo.findByName("test3")

        makeService.formGroups()

        assertNull(lowSkillUser!!.groupRef)

        assertNotNull(midSkillUser!!.groupRef)
        assertFalse(midSkillUser.groupRef!!.formed)

        assertNull(highSkillUser!!.groupRef)

        userService.addUser("test0", 34.0, 34.0 )
        val additionalUser = userRepo.findByName("test0")

        makeService.formGroups()

        assertNotNull(additionalUser!!.groupRef)
        assertEquals(lowSkillUser.groupRef,midSkillUser.groupRef)
        assertEquals(midSkillUser.groupRef,additionalUser.groupRef)

        assertTrue(additionalUser.groupRef!!.formed)
    }

    @Test
    @Order(5)
    @Transactional()
    open fun testGroupRecalculateForming() {
        log().debug("=====TEST [ 5 ] =====")
        userService.addUser("test1", 10.0, 10.0 )
        userService.addUser("test2", 20.0, 20.0 )
        userService.addUser("test3", 30.0, 30.0 )

        val lowSkillUser = userRepo.findByName("test1")
        val midSkillUser = userRepo.findByName("test2")
        val highSkillUser = userRepo.findByName("test3")

        makeService.formGroups()

        assertNull(lowSkillUser!!.groupRef)
        assertNotNull(midSkillUser!!.groupRef)
        assertNull(highSkillUser!!.groupRef)

        userService.addUser("test4", 100.0, 100.0 )

        makeService.formGroups()

        assertNull(lowSkillUser.groupRef)
        assertNotNull(midSkillUser.groupRef)
        assertNull(highSkillUser.groupRef)

        userService.calculateSkillRateDifference()

        makeService.formGroups()

        assertNotNull(lowSkillUser.groupRef)
        assertNotNull(midSkillUser.groupRef)
        assertNotNull(highSkillUser.groupRef)

        assertTrue(midSkillUser.groupRef!!.formed)
    }
}