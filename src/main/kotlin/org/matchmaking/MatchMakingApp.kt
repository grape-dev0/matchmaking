@file:JvmName("Matchmaking")
package org.matchmaking

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class MatchMakingApp

fun main(args : Array<String>) {
    runApplication<MatchMakingApp>(*args)
}