package org.matchmaking.util

import java.util.*

object Format{
    const val DOUBLE_FORMAT = "%8.2f"
    const val SEC_L_FORMAT = "%4d sec"
    const val SEC_D_FORMAT = "%7.2f sec"

    fun StringBuilder.appendf(pattern : String, vararg args : Any?) : StringBuilder {
        return append(pattern.format(*args))
    }

    fun Date?.secondsFrom() = (System.currentTimeMillis() - (this?.time ?: System.currentTimeMillis())) / 1000
}