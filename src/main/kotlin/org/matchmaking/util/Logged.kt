package org.matchmaking.util

import org.slf4j.LoggerFactory

interface Logged {
    fun log() = LoggerFactory.getLogger(this::class.java)
}