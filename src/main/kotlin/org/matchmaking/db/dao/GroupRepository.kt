package org.matchmaking.db.dao

import org.matchmaking.db.pojo.Group
import org.springframework.data.domain.Sort
import org.springframework.data.repository.CrudRepository

interface GroupRepository : CrudRepository<Group, Long> {
    fun findAllByFormedFalse(sort : Sort) : MutableCollection<Group>
}