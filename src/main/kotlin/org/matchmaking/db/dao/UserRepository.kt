package org.matchmaking.db.dao

import org.matchmaking.db.pojo.User
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRepository : JpaRepository<User, Long> {
    fun findByName(name : String) : User?

    fun findAllByGroupRefIsNull(sort : Sort) : MutableCollection<User>

    @Query("select AVG(ut.${User.SKILL}) from user_table ut", nativeQuery = true)
    fun getAvgSkill() : Double?

    @Query("select (max(ut.${User.SKILL}) - min(ut.${User.SKILL}))/count(ut.${User.ID}) from user_table ut",
        nativeQuery = true)
    fun getAvgSkillStep() : Double?

    @Query("select AVG(ut.${User.LATENCY}) from user_table ut", nativeQuery = true)
    fun getAvgLatency() : Double?

    @Query("select (max(ut.${User.LATENCY}) - min(ut.${User.LATENCY}))/count(ut.${User.ID}) from user_table ut",
        nativeQuery = true)
    fun getAvgLatencyStep() : Double?
}
