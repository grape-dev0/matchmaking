package org.matchmaking.db.dao.impl

import org.matchmaking.db.dao.GroupRepository
import org.matchmaking.db.pojo.Group
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.data.domain.Sort

@Repository
open class GroupDAO {

    @Autowired
    lateinit var repository : GroupRepository

    open fun findAllByFormedFalse(sort : Sort) = repository.findAllByFormedFalse(sort)

    open fun save(group : Group) : Group = repository.save(group)
    open fun delete(group : Group) = repository.delete(group)

}