package org.matchmaking.db.dao.impl

import org.matchmaking.db.dao.UserRepository
import org.matchmaking.db.pojo.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.criteria.Root

@Repository
open class UserDAO {
    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var em : EntityManager

    @Transactional(isolation = Isolation.READ_COMMITTED)
    open fun getWaitedUsers() = repository.findAllByGroupRefIsNull(Sort.by(Sort.Direction.ASC, User.REGISTER_DATE))
    open fun existsUserWithName(name : String) : Boolean = repository.findByName(name) != null
    /*
     *  Delegating
     */
    open fun save(user : User) : User = repository.save(user)
    open fun delete(user : User) = repository.delete(user)

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    open fun getAvgSkill() : Double? = repository.getAvgSkill()
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    open fun getAvgSkillStep() = repository.getAvgSkillStep()

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    open fun getAvgLatency() : Double? = repository.getAvgLatency()
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    open fun getAvgLatencyStep() = repository.getAvgLatencyStep()

}
