package org.matchmaking.db.pojo

import java.util.*
import javax.persistence.*


@Entity(name = Group.TABLE_NAME)
@Table(name = Group.TABLE_NAME,
indexes = [
    Index(name = "${Group.FORMED}_idx", columnList = Group.FORMED)
])
@SequenceGenerators( value = [
    SequenceGenerator(
        name = Group.SEQUENCE_NAME,
        sequenceName = Group.SEQUENCE_NAME,
        initialValue = 1)
])
open class Group {
    companion object Constants {
        const val TABLE_NAME = "groupTable"

        const val ID = "id"
        const val SERIAL_NUMBER = "serialNumber"
        const val AVG_SKILL = "avgSkill"
        const val AVG_LATENCY = "avgLatency"
        const val FORMED = "formed"
        const val REGISTER_DATE = "registerDate"

        const val SEQUENCE_NAME = "${TABLE_NAME}Seq"
    }

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    open var id : Long? = null

    @Column(name = SERIAL_NUMBER)
    open var serialNumber : Long? = null

    @Column(name = AVG_SKILL)
    open var avgSkill : Double = 0.0

    @Column(name = AVG_LATENCY)
    open var avgLatency : Double = 0.0

    @Column(name = FORMED)
    open var formed : Boolean = false

    @Column(name = REGISTER_DATE)
    open var registerDate : Date = Calendar.getInstance().time

    @OneToMany(fetch = FetchType.EAGER,
        targetEntity = User::class,
        cascade = [CascadeType.ALL],
        mappedBy = User.GROUP_REF
    )
    open var users : MutableSet<User> = hashSetOf()
}