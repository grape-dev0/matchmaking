package org.matchmaking.db.pojo

import java.util.*
import javax.persistence.*

@Entity(name = User.TABLE_NAME)
@Table(name = User.TABLE_NAME)
@SequenceGenerator(
    name = User.SEQUENCE_NAME,
    sequenceName= User.SEQUENCE_NAME,
    initialValue = 1)
open class User {
    companion object Constants {
        const val TABLE_NAME = "userTable"

        const val ID = "id"
        const val GROUP_REF = "groupRef"
        const val NAME = "name"
        const val SKILL = "skill"
        const val LATENCY = "latency"
        const val REGISTER_DATE = "registerDate"
        const val ACTION_DATE = "actionDate"

        const val SEQUENCE_NAME = "${TABLE_NAME}Seq"
    }

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    open var id : Long? = null

    @ManyToOne
    @JoinColumn(name= GROUP_REF, nullable=true)
    open var groupRef : Group? = null

    @Column(name = NAME)
    open var name : String? = null

    @Column(name = SKILL)
    open var skill : Double = 0.0

    @Column(name = LATENCY)
    open var latency : Double = 0.0

    @Column(name = REGISTER_DATE)
    open var registerDate : Date = Calendar.getInstance().time

    @Column(name = ACTION_DATE)
    open var actionDate : Date? = null
}
