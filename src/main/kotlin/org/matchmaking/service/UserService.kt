package org.matchmaking.service

import org.matchmaking.db.dao.impl.UserDAO
import org.matchmaking.db.pojo.Group
import org.matchmaking.db.pojo.User
import org.matchmaking.util.Format.appendf
import org.matchmaking.util.Format.DOUBLE_FORMAT
import org.matchmaking.util.Format.SEC_L_FORMAT
import org.matchmaking.util.Logged
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
open class UserService : Logged {
    @Autowired
    lateinit var userDAO : UserDAO

    @Autowired
    lateinit var rateService : RateService

    /**
     * Try to add user by request to database
     */
    @Transactional(isolation = Isolation.READ_COMMITTED)
    open fun addUser(name : String, skill : Double, latency : Double) : String {
        return if (userDAO.existsUserWithName(name)) {
            "UserAlreadyExists"
        } else {
            val user = User()
            user.name = name
            user.skill = skill
            user.latency = latency
            user.registerDate = Date()
            userDAO.save(user)

            log().debug("User(name = {}, skill = {}, latency = {}  added = true)",
                user.name, user.skill, user.latency)
            "UserAdded"
        }
    }

    open fun leave(user : User) {
        log().debug("User(name = {}, join = {}",
            user.name, user.groupRef?.id ?: "null")

        user.groupRef = null
        userDAO.save(user)
    }
    open fun join(user : User, group : Group) {
        leave(user)
        user.groupRef = group

        userDAO.save(user)
    }

    open fun applyUser(user : User) {
        user.actionDate = Calendar.getInstance().time
        userDAO.save(user)
    }

    open fun sortByRateTime(users : MutableCollection<User>) : Collection<User> =
        users.sortedWith( compareBy { rateService.rateT(it) } )

    open fun getWaitedUsers() = userDAO.getWaitedUsers()

    /**
     * Filter already joined into groups users
     *
     * @param waitedUsers list of users
     */
    open fun filterJoined(waitedUsers : MutableCollection<User>) = waitedUsers.removeIf { it.groupRef != null }

    open fun printUsers(users : Collection<User>) : String {
        val sb : StringBuilder = StringBuilder()
        sb.append("[\n")
        for (user in users) {
            sb.append(printUsers(user))
            sb.append( if (users.indexOf(user) != users.size -1 ) ",\n" else "]" )
        }
        return sb.toString()
    }
    open fun printUsers(user : User) : String {
        val sb = StringBuilder()
        with(user) {
            sb.append("\t")
                .append("{\n\t")
                .append("name: $name")
                .append("\n\t")
                .append("skill:").appendf(DOUBLE_FORMAT, skill)
                .append("\n\t")
                .append("latency:").appendf(DOUBLE_FORMAT, latency)
                .append("\n\t")
                .append("registered: $registerDate")
                .append("\n\t")
                .append("joined: $actionDate")
                .append("\n\t")
                .append("timeInQueue:").appendf(SEC_L_FORMAT, user.registerDate.secondsFrom())
                .append("\n\t")
                .append("rate").appendf(DOUBLE_FORMAT, rateService.rate(this))
                .append("\n\t}")
        }
        return sb.toString()
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    open fun calculateSkillRateDifference() {
        val avgSkill = userDAO.getAvgSkill()
        val avgLatency = userDAO.getAvgLatency()

        val avgSkillStep = userDAO.getAvgSkillStep() ?: 0.0
        val avgLatencyStep = userDAO.getAvgLatencyStep() ?: 0.0

        log().debug("Calculate( avgSkill = {}, avgLatency = {}, avgSkillStep = {}. avgLatencyStep = {}",
            avgSkill, avgLatency, avgSkillStep, avgLatencyStep)
        if (avgSkill != null && avgLatency != null) {
            val skillRate = avgSkill / (avgLatency + avgSkill)
            val latencyRate = avgLatency / (avgLatency + avgSkill)

            rateService.config((avgSkillStep + avgLatencyStep) / 2, skillRate, latencyRate)
        }
    }
}