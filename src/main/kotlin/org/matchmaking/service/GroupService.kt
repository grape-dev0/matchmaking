package org.matchmaking.service

import org.matchmaking.db.dao.impl.GroupDAO
import org.matchmaking.db.pojo.Group
import org.matchmaking.db.pojo.User
import org.matchmaking.util.Format.DOUBLE_FORMAT
import org.matchmaking.util.Format.SEC_D_FORMAT
import org.matchmaking.util.Format.SEC_L_FORMAT
import org.matchmaking.util.Format.appendf
import org.matchmaking.util.Logged
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors
import kotlin.streams.toList


@Service
open class GroupService(
    @Value("\${groupSize}") var groupSize : Int,
    @Autowired var groupDAO : GroupDAO,
    @Autowired var rateService: RateService,
    @Autowired var userService : UserService
) : Logged {

    open fun buildByAverage(users : Collection<User>) : Group {
        val cAvgSkill = users.stream().collect( Collectors.averagingDouble{ it.skill } )
        val cAvgLatency = users.stream().collect( Collectors.averagingDouble{ it.latency } )
        val cAvgRegisterTime = users.stream().collect( Collectors.averagingLong{ it.registerDate.time } ).toLong()

        return with(Group()) {
            avgSkill = cAvgSkill
            avgLatency = cAvgLatency
            registerDate = Date(cAvgRegisterTime)
            formed = false
            this
        }
    }
    open fun applyGroup(group : Group) {
        group.formed = true
        group.serialNumber = group.id

        groupDAO.save(group)

        for (user in group.users) {
            userService.applyUser(user)
        }

        println( printGroup(group) )
    }

    /**
     * Try to delete group instance if their user list is empty (after merge)
     */
    open fun autoDelete(group : Group) : Boolean {
        if (group.users.isEmpty()) {
            groupDAO.delete(group)
            return true
        }
        return false
    }
    /**
     * Join user to group
     * Recalculate avg properties in group
     * Reassign user references
     *
     * @param group group for joining
     * @param user user which joined
     */
    open fun join(user : User, group : Group) {
        leave(user)
        group.users.add(user)
        log().debug("User(name = {}, join = {}", user.name, group.id)

        userService.join(user, group)
        recalculateAvgProperties(group)

        groupDAO.save(group)
    }
    /**
     * Leave user from group (on merge, as example)
     * Update group properties
     */
    open fun leave(user : User) {
        val group = user.groupRef ?: return
        recalculateAvgProperties(group)
        group.users.removeIf{ it == user }
        userService.leave(user)
        groupDAO.save(group)
    }

    private fun recalculateAvgProperties(group : Group) {
        with (group) {
            val qty = users.size

            avgSkill = users.stream().mapToDouble { it.skill }.sum() / qty
            avgLatency = users.stream().mapToDouble { it.latency }.sum() / qty

            if (qty == groupSize) {
                group.formed = true
            }
        }
    }

    open fun getAllNotFormedGroupsSortedByDate() =
        groupDAO.findAllByFormedFalse(Sort.by(Sort.Direction.ASC, Group.REGISTER_DATE))


    open fun filterEmpty(groups : MutableCollection<Group>) = groups.removeIf { autoDelete(it) }
    open fun filterFormed(notFormedGroups : MutableCollection<Group>) = notFormedGroups.removeIf { it.formed }

    open fun sortByRate(groups : MutableCollection<Group>) =
        groups.sortedWith( compareBy { rateService.rate(it) } )
    open fun sortByRateTime(groups : MutableCollection<Group>) =
        groups.sortedWith( compareBy { rateService.rateT(it) } )

    open fun filterAndSort(groups : MutableCollection<Group>) {
        filterFormed(groups)
        sortByRate(groups)
    }

    open fun printGroup(group : Group) : String {
        val skillList : List<Double> = group.users.parallelStream().mapToDouble{
            it.skill
        }.toList()
        val minSkill : Double = skillList.stream().min(Double::compareTo).get()
        val maxSkill : Double = skillList.stream().max(Double::compareTo).get()
        val avgSkill : Double = skillList.average()

        val latencyStream = group.users.parallelStream().mapToDouble{
            it.latency
        }.toList()
        val minLatency : Double  = latencyStream.stream().min(Double::compareTo).get()
        val maxLatency : Double  = latencyStream.stream().max(Double::compareTo).get()
        val avgLatency : Double  = latencyStream.average()

        val waitTimeStream = group.users.stream().mapToLong{
            (it.actionDate!!.time - it.registerDate.time ) / 1000
        }.toList()

        val minWaitTime : Long = waitTimeStream.stream().min(Long::compareTo).get()
        val maxWaitTime : Long = waitTimeStream.stream().max(Long::compareTo).get()
        val avgWaitTime : Double  = waitTimeStream.average()

        val result : StringBuilder = StringBuilder()
        return result.append("Group #${group.id} {\n\t")
            .appendf(DOUBLE_FORMAT, minSkill)
            .append(" / ")
            .appendf(DOUBLE_FORMAT, maxSkill)
            .append(" / ")
            .appendf(DOUBLE_FORMAT, avgSkill)
            .append("\n\t")
            .appendf(DOUBLE_FORMAT, minLatency)
            .append(" / ")
            .appendf(DOUBLE_FORMAT, maxLatency)
            .append(" / ")
            .appendf(DOUBLE_FORMAT, avgLatency)
            .append("\n\t")
            .appendf(SEC_L_FORMAT, minWaitTime)
            .append(" / ")
            .appendf(SEC_L_FORMAT, maxWaitTime)
            .append(" / ")
            .appendf(SEC_D_FORMAT, avgWaitTime)
            .append(printUsers(group))
            .toString()
    }

    open fun printUsers(group : Group) : String {
        return userService.printUsers(group.users)
    }
}