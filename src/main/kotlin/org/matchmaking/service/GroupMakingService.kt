package org.matchmaking.service

import org.matchmaking.db.pojo.Group
import org.matchmaking.db.pojo.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.matchmaking.util.Logged
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.math.abs

/**
 * Class for making and completing groups
 *
 * @param groupSize max length for group
 */
@Service
open class GroupMakingService(
    @Value("\${groupSize}") var groupSize : Int,
    @Autowired var rateService : RateService,
    @Autowired var groupService : GroupService,
    @Autowired var userService : UserService
) : Logged {

    /**
     * Tro to form group by list of waited users and partial formed groups
     *
     * Form by 3 steps:
     * 1. handle list of partial formed groups and try to assign user to it
     * 2. handle list of last groups and try to merge it by average rates
     * 3. handle list of last users and after splitting it on few lists - try to create groups and join users to it
     *
     * @return collection of formed on this calling groups
     */
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    open fun formGroups() : Collection<Group> {
        log().debug("Form up group with size={} started at {} ",
            groupSize , Date())

        val notFormedGroups = groupService.getAllNotFormedGroupsSortedByDate()
        val waitedUsers = userService.getWaitedUsers()

        log().debug("Groups: {} , Users: {} ",
            notFormedGroups.size, waitedUsers.size)
        if (waitedUsers.isEmpty())
            return ArrayList()

        val formedGroups = handleJoin(notFormedGroups, waitedUsers)
        formedGroups.addAll( handleMerge(notFormedGroups) )
        formedGroups.addAll( handleCreate(waitedUsers as MutableList<User>)  )

        return formedGroups
    }


    /**
     * Join users to exited groups
     * Filter list of users after each group forming
     *
     * @param notFormedGroups - list of groups candidates for forming
     * @param waitedUsers - list of user candidates for joining in groups
     *
     * @return list of formed groups
     */
     open fun handleJoin(notFormedGroups : Collection<Group>, waitedUsers : MutableCollection<User>) : MutableCollection<Group> {
        val formedGroups = hashSetOf<Group>()
        for (group in notFormedGroups) {
            handleJoin(group, waitedUsers, formedGroups)
            userService.filterJoined(waitedUsers)
        }
        return formedGroups
    }

    /**
     * Join user to exited group
     *
     * @param group - group candidates for forming
     * @param waitedUsers list of user candidates for joining in groups
     * @param formedGroups list os already formed groups (for adding on forming)
     *
     */
    open fun handleJoin(group : Group, waitedUsers : MutableCollection<User>, formedGroups: MutableCollection<Group> ) {
        for (user in waitedUsers) {
            if ( rateService.isAssignable(user, group) ) {
                groupService.join(user, group)
                if (tryToApplyGroup(group, formedGroups)) break
            }
        }
    }
    /**
     * Merge partial formed group between themselves
     * Filter list of not formed groups for each group merging
     */
    open fun handleMerge(notFormedGroups : MutableCollection<Group>) : MutableCollection<Group> {
        groupService.filterAndSort(notFormedGroups)
        val formedGroups = hashSetOf<Group>()
        while (!notFormedGroups.isEmpty()) {
            val group : Group = notFormedGroups.first()
            notFormedGroups.removeIf { it == group }

            for (otherGroup in notFormedGroups) {
                if (rateService.isCanBeMerged(group, otherGroup) &&
                    group.users.size + otherGroup.users.size <= groupSize) {

                    mergeGroup(group, otherGroup)
                    if (tryToApplyGroup(group, formedGroups)) break
                }
            }
            groupService.filterEmpty(notFormedGroups)
            groupService.filterFormed(notFormedGroups)

            for (otherGroup in notFormedGroups) {
                val usersFromMergedGroup = ArrayList(userService.sortByRateTime(otherGroup.users))
                for (user in otherGroup.users) {
                    if (group.users.size == groupSize || usersFromMergedGroup.isEmpty()) break

                    val outerUser = usersFromMergedGroup.first()
                    val uRate = rateService.rate(outerUser)
                    if (abs(rateService.diff(group) - uRate) < abs(rateService.diff(otherGroup) - uRate)) {
                        usersFromMergedGroup.removeIf { it == outerUser }
                        groupService.join(outerUser, group)
                    }
                }
            }
            groupService.filterEmpty(notFormedGroups)
            groupService.filterFormed(notFormedGroups)
        }
        return formedGroups
    }

    /**
     * Merge two groups
     * Try to join users from second groups and join it ti first
     *
     * @param group first group for merging
     * @param otherGroup second group for merging
     */
    open fun mergeGroup(group : Group, otherGroup : Group) {
        val usersFromMergedGroup = ArrayList(userService.sortByRateTime(otherGroup.users))
        while (group.users.size < groupSize && usersFromMergedGroup.isNotEmpty()) {
            val outerUser = usersFromMergedGroup.first()
            usersFromMergedGroup.removeIf { it == outerUser }

            groupService.join(outerUser, group)
        }
    }
    /**
     * Creating groups with lasted users
     * Divide list on sub lists with groupSize length and create groups with their AVG properties
     * Possible last few (qty < groupSize) users which wasn't handled
     * Groups will not formed if users less then groupSize
     *
     * @param waitedUsers list of users for splitting by groups
     */
    open fun handleCreate(waitedUsers : MutableList<User>) : MutableCollection<Group> {
        val groupCount : Int = waitedUsers.size / groupSize
        val formedGroups = hashSetOf<Group>()

        log().debug("Handle(groupSize = {}, groupCount = {})", groupSize, groupCount)
        if (groupCount == 0) return ArrayList()

        userService.sortByRateTime(waitedUsers)
        for (i in 0 until groupCount) {
            val subListOfUser = ArrayList<User>()
            for ( x in 0 until groupSize) {
                subListOfUser.add( waitedUsers[x] )
            }
            waitedUsers.removeAll { subListOfUser.contains(it) }

            val group = groupService.buildByAverage(subListOfUser)
            handleJoin(group, subListOfUser, formedGroups)
        }

        return formedGroups
    }

    /**
     * Try to complete group forming
     *
     * @param group candidate for forming
     * @param formedGroups list os already formed groups (for adding on forming)
     *
     * @return status of forming
     */
    private fun tryToApplyGroup(group : Group, formedGroups : MutableCollection<Group>) : Boolean {
        if (group.users.size == groupSize) {
            applyGroup(group, formedGroups)
        }
        return group.formed
    }

    /**
     * Complete group forming
     *
     * @param group candidate for forming
     * @param formedGroups list os already formed groups (for adding on forming)
     *
     * @return status of forming
     */
    private fun applyGroup(group : Group, formedGroups : MutableCollection<Group>) {
        groupService.applyGroup(group)
        formedGroups.add(group)
    }


    /**
     * Filter formed groups list and sort them by rate with time
     *
     * @param groups list of groups
     */




}