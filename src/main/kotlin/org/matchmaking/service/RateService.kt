package org.matchmaking.service

import org.matchmaking.db.pojo.Group
import org.matchmaking.db.pojo.User
import org.matchmaking.util.Format.secondsFrom
import org.matchmaking.util.Logged
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*
import kotlin.math.abs

/**
 * Class for calculating rate for entities
 */
@Service
open class RateService (
    /**
     * Maximum accepted difference between rating for applying rates (basic value)
     */
    private var rateStep : Double = 5.0,
    /**
     * Time in seconds from start group forming when rateStep will be increased (basic value)
     */
    @Value("\${timeStep}")
    private var timeStep : Int,
    /**
     * Scale for increasing rateStep by linear dependency (basic value)
     */
    @Value("\${timeRate}")
    private var timeRate : Double,
    /**
     * Rate for scale in calculating (basic value)
     */
    private var skillRate : Double = 0.8,
    /**
     * Rate for latency (basic value)
     */
    private var latencyRate : Double = 0.2
): Logged {
    open fun config(rateStep: Double?, skillRate: Double?, latencyRate: Double?) {
        this.skillRate = skillRate ?: this.skillRate
        this.latencyRate = latencyRate ?: this.latencyRate
        this.rateStep = rateStep ?: this.rateStep

        log().debug("Config: skill - [{}], latency - [{}], step - [{}]",
            this.skillRate, this.latencyRate, this.rateStep)
    }
    /**
     * Calculating rate for skill and latency with multiplier
     */
    private fun rate(skill : Double, latency : Double) : Double  =
        skill * skillRate + latency * latencyRate

    open fun rate(group : Group) : Double {
        val rate = rate(group.avgSkill, group.avgLatency)
        log().debug("Group(id = {}, rate = {})", group.id, rate)
        return rate
    }
    open fun rate(user : User) : Double {
        val rate = rate(user.skill, user.latency)
        log().debug("User(name = {}, rate = {})", user.name, rate )
        return rate
    }

    /**
     * Calculate rate with time
     */
    open fun rateT(user : User) : Double = rate(user) + (timeRate * user.registerDate.secondsFrom() / timeStep)
    open fun rateT(group : Group) : Double = rate(group) + (timeRate * group.registerDate.secondsFrom() / timeStep)
    open fun diff(group : Group) : Double {
        val diff = diff(group.registerDate)
        log().debug("Group(id = {}, diff = {})", group.id, diff)
        return diff
    }
    open fun diff(registerDate : Date) : Double  =
        rateStep + timeRate * (registerDate.secondsFrom() / timeStep)

    open fun isAssignable(user : User, group : Group) : Boolean {
        return abs(rate(group) - rate(user)) < diff(group)
    }

    open fun isCanBeMerged(group : Group, other : Group) : Boolean {
        val oldestGroup = if (group.registerDate.after(other.registerDate)) group else other
        return abs( rate(group) - rate(other) ) < diff( oldestGroup )
    }
}