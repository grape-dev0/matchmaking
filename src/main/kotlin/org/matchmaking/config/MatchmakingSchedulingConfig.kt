package org.matchmaking.config

import org.matchmaking.service.GroupMakingService
import org.matchmaking.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional

@Configuration
@EntityScan("org.matchmaking.db.pojo")
@EnableJpaRepositories(basePackages = ["org.matchmaking.db.dao"])
open class MatchmakingDbConfig

@Configuration
@EnableScheduling
@EnableAutoConfiguration
open class MatchmakingSchedulingConfig {

    @Autowired
    lateinit var makeService : GroupMakingService

    @Autowired
    lateinit var userService: UserService

    @Scheduled(fixedDelay = 5 * 1000)
    open fun formGroupsScheduled() {
        makeService.formGroups()
    }

    @Scheduled(fixedDelay = 60 * 1000)
    open fun recalculateRates() {
        userService.calculateSkillRateDifference()
    }
}