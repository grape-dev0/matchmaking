package org.matchmaking.api

open class UserRequest (
    var name : String,
    var skill : Double,
    var latency : Double
)