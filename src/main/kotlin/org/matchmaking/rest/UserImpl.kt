package org.matchmaking.rest

import org.matchmaking.api.UserRequest
import org.matchmaking.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/")
open class UserImpl {
    @Autowired
    lateinit var userService : UserService

    @PostMapping("/users")
    open fun users (
        @RequestParam name : String,
        @RequestParam skill : Double,
        @RequestParam latency : Double ) : String {
        return try {
            userService.addUser(name, skill, latency)
        } catch( exp : Exception ) {
            exp.localizedMessage
        }
    }
    @PostMapping("/usersJ")
    open fun usersJSON (
        @RequestBody request : UserRequest
    ) : String {
        return try {
            with (request) { userService.addUser(name, skill, latency) }
        } catch( exp : Exception ) {
            exp.localizedMessage
        }
    }
}